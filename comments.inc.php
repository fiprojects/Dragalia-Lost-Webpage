
<?php
function setComment($dbLink){
    if(isset($_POST['submitComment'])){
        $name = $_POST['name'];
        $date = $_POST['date'];
        $comment = $_POST['comment'];
        
        $query = "INSERT INTO comment (name, date, comment) VALUES ('$name', '$date', '$comment')";
        $result = mysqli_query($dbLink, $query);
    }
}

function getComment($dbLink){
    $query = "SELECT * FROM  comment";
    $result = mysqli_query($dbLink, $query);
    while($row = $result -> fetch_assoc()) {
        echo "<div class='user-comment-box default-border default-halftone-effect'>";
            echo "<h3>";
                echo $row['name'];
            echo "</h3>";
            echo "<p>";
                echo $row['date'];
            echo "</p>";
            echo "<p>";
                echo nl2br($row['comment']);
            echo "</p>";
        echo "</div>";
    }
}