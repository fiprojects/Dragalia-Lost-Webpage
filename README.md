# Dragalia-Lost-Webpage
Webpage project made for the 2nd Semester of college for a Web & Mobile Course.
The website was dedicated to the now, shut down, mobile game Dragalia Lost. The game met its end-of-service on the 30th of November 2022.
One of the primary goals with this page was to try to imitate the look and feel of the original game's style.

The Discussion webpage does not work as the original site that hosted the project is not currently running.
Originally, this page featured the ability for visiting users to the webpage to leave comments.
The code relating to this page still remains in the project for observation/learning purposes.


©2019 Filipa Ivanković
    Dragalia Lost belongs to their respective owners.

