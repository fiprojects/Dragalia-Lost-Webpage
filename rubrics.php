<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="//db.onlinewebfonts.com/c/527d17cddcb5f301ba9400f40aaf3d84?family=Avalon" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="Images/Midgardsorm_icon.ico">
    <title>Dragalia Lost Rubrics</title>
    <style>
        body{
            font-size: 15px !important;
        }
    </style>
</head>
<body>
    <?php
    include('./View/shared/navigation.php');
    echo "
    <section class='bottom-margin top-margin'>
        <div class='text-box default-border'>
            <div class='container'>
                <h2>Final Individual Project Changes</h2>
                <h3>Original JavaScript Component</h3>
                <p>
                    On the main homepage (index.php), I have added a video with custom made button controls that are located on it.
                    There are two buttons. One pauses/plays the video, and the other plays/mutes the video's sound.
                    Both of these things are achieved using JavaScript.
                    Hovering over them changes their opacity and some other properties.
                </p>
                <h3>DHTML Component</h3>
                <p>
                    On the stages.php page, near the bottom of the page, before the footer, in the 'Advanced Dragon Trials' section, is the DHTML component.
                    After 5 seconds, the picture of the dragon, located on the right side of the div, will change to a different picture of another dragon.
                    There are 3 dragons in total. This component was implemented to showcase all the currently available dragons for that perticular stage.
                </p>
                <h3>Corrections from the midterm project</h3>
                <p>
                    Since I didn't receive any point deductions or complaints on my Midterm project, I didn't change that many things.
                    The biggest change would be the switch from .html to .php on all pages to support the requirement of having php code on each page.
                    I changed some text that was outdated, due to the game getting new updates. I also re-located the main header wallpaper from the index page to the adventurers page to make room for the new video.
                    More CSS was added to support the websites scaling and look on mobile.
                </p>
                <h3>Comment form</h3>
                <p>
                    The comment form is located on under the 'Discussions' nav-item. Users are able to write their name/username and a comment.
                    They can write whatever they want, as long as it is not blank/empty. On top of the username and comment being displayed, the time when the comment was posted is also displayed.
                    The time and date are based on the Europe/Zagreb time. The background behind the comment boxes was made in Photoshop.
                </p>
            </div>
        </div>

    </section>
    
    
    <section class='bottom-margin top-margin'>
      <div class='text-box default-border'>
        <div class='container'>
          <h2>Present on all pages</h2>
          <p>
              All pages use a Bootstrap navbar. The background image (Poly_Pattern) was made by me in Photoshop.
              I also added the games Logo into the navbar.
          </p>
          <p>
              At the bottom of each page is a footer section. It contains some basic copyright stuff and a link to
              my student portfolio.
          </p>
          <p>
              Throughout the webpage, I use the font 'Avalon'. The reason why is because the game Dragalia Lost uses
              that font and I wanted the page to aesthecally resemble the official game's UI.
          </p>
          <p>
              In the title bar, using one of the game's Dragon artwork, I created a small icon (Photsohop & GIMP).
          </p>
          <p>
              The Color Halftone (the green dot effect) were used on all divs. This type of an effect is also used in
              the game's UI. I made my own (Photoshop) halftone effect to make the divs and paragraphs resemble the games. 
          </p>
          <p>
              The colors that repeat through the page are green and blue. Red,blue,darker green,purple and yellow are used to represent
              the Elemental chart within the game. Red represents fire, blue water, darker green wind, purple shadow and yellow light.
          </p>
        </div>
      </div>
    </section>";
    ?>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border">
        <div class="container">
          <h2>index.php</h2>
          <p>
              This is the main page. At the top is the wallpaper which uses a Color #00284d border.
              The first text section states some background information about the production of the game, supported languages and devices and the commpanies behind the.
          </p>
          <p>
              The second section is about the gameplay. Some general information is given, while a more in-depth look into this is provided by the rest of the pages.
              I also used a video here, which I recorded on my Iphone. This video showcases a more simple stage using a fully developed team.
          </p>
          <p>
              Last section is about the in-game purchases that are present within the game.
              A Result screenshot of the Summoning process is provided here.
          </p>
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border">
        <div class="container">
          <h2>adventurers.php</h2>
            <p>
              This page is all about the playable characters known as Adventurers.
            </p>
            <p>
              The first section gives a basic description of what each character has at their disposal, the weapon classes and an image of the Elemental chart.
            </p>
            <p>
              Following after that are 8 sections with each focusing on a specific weapon class.
              Here I describe each classes strengths and weaknesses, while giving out some tips on what kind of a player might be attracted to this classes particular playstyle and some tips to use in-game.
              Each section uses an image of an Adventurer of that particular weapon class.
            </p>
            <p>
              The sections are colored (background image & border) in the Elemental colors to represent that Adventurer's Elemental alignment (2 fire,2 water, 2 wind, 1 shadow and 1 light).
              To make the sections look a little more dynamic, some sections are aligned to fit the left side of their div, while others the right side.
              Adventurer images are flipped horizontally using the {transform: scaleX(-1);} command, while the background images had to be edited manually within Photoshop.
            </p>
            <p>
                Adventurer images also have a {drop shadow} effect on them to make them further stnd out from the background.
            </p>
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border">
        <div class="container">
        <h2>dragons.php</h2>
            <p>
                At the top of the page is a wallpaper similiar to the one on the homepage.
                More wallpaper would have been used on the other pages, but because this game is still particularly new, there isn't a lot of wallpaper worthy artwork.
            </p>
            <p>
                This page focuses on the second playable set of characters, the Dragons.
                How Dragons work & play, how to raise them & level them is all explained here.
            </p>
            <p>
                An image of the Shadow Dragon Nidhogg is used here, a screenshot of the Unbinding process and a screenshot for the Dragon's Roost.
                The dragon image is given a {drop shadow} effect.
            </p>
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border">
        <div class="container">
          <h2>stages.php</h2>
          <p>
              This page is about all the types of stages the player might find in Dragalia Lost.
          </p>
          <p>
              The first section is about the Campaign aspect of the game. The story mode.
              A screenshot of a story chapter is provided here.
          </p>
          <p>
              In the Avenue section and Dragon Trials section, images of these particular stages where placed
              in a grid. All of these images showcase the type of foes the player will encounter in them.
          </p>
          <p>
              The Dragon Trials and Imperial Onslaught sections also contain 2 videos, which showcase these stages.
              I tried to keep these as short as possible to keep the file sizes smaller.
          </p>
          <p>
              Last section is about the most difficult content in the game: the Advanced Dragon Trials.
              An in-depth guide is given here on how to unlock these stages as the process is quite lengthy.
              An image of the Advanced Dragon boss is used here, which also uses the {drop shadow} effect.
          </p>
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border">
        <div class="container">
            <h2>castle.php</h2>
            <p>
                The Castle page is about the home base of the player. 
                Information about the facilities one may build here is given & what type of effect each facility has.
            </p>
            <p>
                Some screenshots for these facilities is provided and a few images aswell.
            </p>
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border">
        <div class="container">
            <h2>wyrmprint.php</h2>
            <p>
                Lastly, the Wyrmprint page is about the Wyrmprint cards that play an important role for team building.
                This is the shortest section as Wyrmprints are pretty simple.
            </p>
            <p>
                A differnt div layout is used to showcase an example of a Wyrmprint.
            </p>
        </div>
      </div>
    </section>
    

    <?php
      include('./View/shared/footer.php');
    ?>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>