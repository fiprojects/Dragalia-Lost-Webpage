<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="//db.onlinewebfonts.com/c/527d17cddcb5f301ba9400f40aaf3d84?family=Avalon" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="Images/Midgardsorm_icon.ico">
    <title>Dragalia Lost Adventurers</title>
</head>
<body>

    <?php
    include('./View/shared/navigation.php');
    echo "
        <img class='img-fluid wallpaper' src='Images/Wallpaper.png' alt='Dragalia Lost Wallpaper'>";
    ?>
    
    <section class="top-margin bottom-margin">
        <div id="Adventurers" class="text-box default-halftone-effect default-border">
          <div class="container">
            <h2>Adventurers</h2>
            <p>Adventurers are the playable characters which form the player's party.
              Adventurers come in 5 different elements (<span class="color-fire">fire</span>,<span class="color-wind">wind</span>,<span class="color-water">water</span>,<span class="color-light">light</span>,<span class="color-shadow">dark</span>) and 8 weapon classes.</p>
            <p>Each Adventurer has primary and secondary ability that may be used in battle.</p>
            <p>Depending on the weapon class, an Adventurer will provide different bonuses to the party. These bonuses are called Co-abilities.
              These Co-abilites can be enhanced with <a href="index.html#Summoning">Eldwater</a>.</p>
            <p>A party can have up to 4 Adventurers.
              In singleplayer mode an additional support Adventurer may be chosen from a friend-listed player, however, only their primary ability may be used in battle.</p>
          </div>
            <div class="container">
              <div class="row">
                <div class="col-sm"></div>
                <div class="col-sm">
                  <img id="Elements" class="img-fluid img-thumbnail" src="Images/Elements.jpg" alt="Element chart">
                  <p>In the Elemental chart, the arrows indicate which element is effective against which.</p>
                </div>
                <div class="col-sm"></div>
              </div>
            </div>
        </div>
    </section>

    <section class="top-margin bottom-margin">
      <div class="text-box fire-border fire-halftone-effect">
        <img id="Euden" class="img-fluid adventurer-img float-right flip-img" src="Images/Euden.PNG" alt = "Fire Adventurer Euden - Sword class">
        <div class="container">
          <h2>Sword</h2>
          <p>
            Sword weapon class is the most straightforward & simplest weapon class to use.
            As the class with the highest damaging force strikes, the sword class is excellent for dealing with a bosses and hordes of enemies.
            Decent attacking speed and good damage, make the Sword weapon class a reliable choice.
            However, it is good to note that the Sword class has the lowest range out of all the other weapon classes.</p>
          <p>
            Co-ability: Increases <a href="dragons.html#Dragons">dragon gauge</a> fill rate for the entire team.</p>
          <p>
            Recommended for newer players.
            More experienced players will find their fast force strikes very useful for clearing bosses.</p>
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box shadow-border shadow-halftone-effect text-align-right">
        <img id="Ieyasu" class="img-fluid adventurer-img float-left" src="Images/Ieyasu.png" alt = "Shadow Adventurer Ieyasu - Blade class">
        <div class="container">
          <h2>Blade</h2>
          <p>
            The Blade weapon class is heavily geared towards an offensive playstyle.
            This class is a great addition to every team as their Co-ability increases every characters Strength values.
            This class is the toughest one to play correctly as it lacks good defensive options and comboing between normal attacks and force strikes is a must.
            It has the same attack speed as the Sword class, but the range is much greater.</p>
          <p>
            Co-ability: Increases Strength for the entire team.</p>
            <h3>Tips</h3>
            <p>
            Recommended for more experienced players.
            Having a Blade Adventurer in your party will significantly decrease the time you need to clear a stage.</p>
        </div>
      </div>
    </section>
    
    <section class="bottom-margin top-margin">
      <div class="text-box water-border water-halftone-effect text-align-left">
        <img id="Elisanne" class="img-fluid adventurer-img float-right" src="Images/Elisanne.png" alt="Water Adventurer Elisanne - Lance class">
        <div class="container">
          <h2>Lance</h2>
          <p>
            The Lance weapon class is the best melee class for boss battling.
            High single target damage and the highest range among the other melee weapon classes is what makes this class a fearsome boss slayer.
            The only weakness of this class is that it struggles against hordes of enemies.
            Overall, the Lance class is a very strong class.</p>
          <p>
            Co-ability: Increases maximum Health Points for the entire team.
          </p>
          <h3>Tips</h3>
          <p>
            Recommended for newer players.
            Bringing a Lance Adventurer to a tougher stage will increase a team's survivability.</p>
        </div>
      </div>
    </section>

    <section class="top-margin bottom-margin">
      <div class="text-box wind-border wind-halftone-effect-flip text-align-right">
        <img id="Hawk" class="img-fluid adventurer-img float-left flip-img" src="Images/Hawk.png" alt="Wind Adventurer Hawk - Bow class">
        <div class="container">
          <h2>Bow</h2>
          <p>
            The Bow weapon class utilizes range and fast attacking speed to deal high single-target burst damage.
            It is great for taking on bosses, but it struggles against multiple enemies, because a bow Adventurer may only attack one target at a time.
            Their force strike is a medium sized area attack that rain arrows on foes. 
            To master the bow class, one needs to learn how to properly position themselves and avoid getting hit as the bow class rewards a careful approach to fighting.</p>
          <p>
            Co-ability: Increases skill gauge fill rate for the entire team.
          </p>
            <h3>Tips</h3>
            <p>
            Recommended for players who want to play a ranged class.
            Avoid bringing Bow Adventurers to narrow stages, because their projectiles will be negated when they hit an obstacle or wall.</p>
        </div>
      </div>
    </section>

    <section class="top-margin bottom-margin">
      <div class="text-box water-border water-halftone-effect text-align-left">
        <img id="Dragonyule_Cleo" class="img-fluid adventurer-img float-right" src="Images/Dragonyule_Cleo.png" alt="Water Adventurer Dragonyule Cleo - Dagger class">
        <div class="container">
          <h2>Dagger</h2>
          <p>
            The Dagger class is represented by fast attacking speed and mobility on each attack.
            This class relies heavily on combos to deal the most possible amount of damage and continuously chaining attacks.
            It a class heavily suited towards offense, as Dagger Adventurers tend to have lower health pools.
            It has the same attack range as the Blade weapon class and decent area coverage.</p>
          <p>
            Co-ability: Increases critical rate (chance to score a critical hit) for the entire team.
          </p>
            <h3>Tips</h3>
            <p>
            Recommended for players who enjoy fast movement and combos.
            Learning to properly dodge roll with this class is of great importance, as every time you get hit your combo counter will reset back to 0.</p>
        </div>
      </div>
    </section>

    <section class="top-margin bottom-margin">
      <div class="text-box fire-border fire-halftone-effect-flip text-align-right">
        <img id="Vanessa" class="img-fluid adventurer-img float-left flip-img" src="Images/Vanessa.png" alt="Fire Adventurer Vanessa - Axe class">
        <div class="container">
          <h2>Axe</h2>
          <p>
            The Axe weapon class is the bruiser/tank class in Dragalia Lost.
            Great area coverage with normal attacks, and force strikes that cover a massive radius around the Adventurer.
            This class is great against hordes of enemies and can do very well against bosses; however, it struggles against projectile based opponents.
            The Axe class biggest weakness is their decreased ability charging rate, due to the fact that their attacking speed is pretty slow. 
          </p>
          <p>
            Co-ability: Increases defense for the entire team.
          </p>
          <h3>Tips</h3>
          <p>
            Recommended for players that enjoy playing aggressively without having to worry too much about their health bar.
            Bringing an Axe adventurer to a tougher stage will increase the team's survivability.</p>
        </div>
      </div>
    </section>

    <section class="top-margin bottom-margin">
      <div class="text-box wind-border wind-halftone-effect text-align-left">
        <img id="Maribelle" class="img-fluid adventurer-img float-right" src="Images/Maribelle.png" alt="Wind Adventurer Maribelle - Wand class">
        <div class="container">
          <h2>Wand</h2>
          <p>
            The Wand weapon class is the mage class in Dragalia Lost.
            This class is defined by their powerful abilities which can wipe entire hordes of enemies or wittle down bosses.
            Their main weakness is their slow projectile speed.
            Along side the Staff class, the Wand has the greatest attack range.
          </p>
          <p>
            Co-ability: Increases attack skill damage.
          </p>
            <h3>Tips</h3>
            <p>
            Recommended for players who want an offensive spellcaster playstyle.</p>
        </div>
      </div>
    </section>
    
    <section class="top-margin bottom-margin">
      <div class="text-box light-border light-halftone-effect text-align-right">
        <img id="Hildegarde" class="img-fluid adventurer-img float-left flip-img" src="Images/Hildegarde.png" alt="Light Adventurer Hildegarde - Staff class">
        <div class="container">
          <h2>Staff</h2>
          <p>
          The Staff class is the support healer class.
          They are the class with the lowest damage output, skill charge rate and area coverage, but they are the only class that can heal the entire team.
          This class may seem weak, but for late-game content,like Advanced Dragon Trials, healers are a must for every team.
          Along side the Wand class, the Staff has the greatest attack range.</p>
        <p>
          Co-ability: Increases the potency of recovery skills for entire team.</p>
        <h3>Tips</h3>
        <p>
          Recommended for players who want to play a supportive playstyle.
          Equipping a Staff Adventurer with a Dragon that boosts Health is a good choice, as a Staff Adventurer's damage output isn't the greatest and the more Health a Staff Adventurer has, the greater their healing output.</p>
        </div>
      </div>
    </section>

  
    <?php
      include('./View/shared/footer.php');
    ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>