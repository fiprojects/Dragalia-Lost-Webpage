<?php
    date_default_timezone_set('Europe/Zagreb');
    include 'dbInfo.inc.php';
    include 'comments.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="//db.onlinewebfonts.com/c/527d17cddcb5f301ba9400f40aaf3d84?family=Avalon" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="Images/Midgardsorm_icon.ico">
    <script src="js/validation.js"></script>
    <title>Dragalia Lost Discussion</title>
</head>
<body>
    <?php
    include('./View/shared/navigation.php');
    echo " 
        <img class='img-fluid wallpaper' src='Images/Discussion_wallpaper.jpg' alt='Dragalia Lost Wallpaper'>
    
       <div class='comment-box default-border default-halftone-effect'>
            <h2 class='padding-left-20px'>Leave a comment & join the discussion!</h2>
            <form class='comment-form-box' name='commentForm' method='POST' action='".setComment($dbLink)."' novalidate>
        
            <div class='form-group'>
              <label for='name'>Name</label>
              <input type='text' name='name' id='name' class='form-control' placeholder='Enter name..' required>


              <input type='hidden' name='date' id='date' class='form-control' value='".date('Y-m-d H:i:s')."'>

              <label for='comment' class='padding-top-10px'>Comment</label>
              <textarea class='form-control' name='comment' id='comment' placeholder='Enter your comment' required></textarea>
            </div>
        
            <input class='btn btn-primary text-center' type='submit' name='submitComment' value='Submit'>

            </form>
        </div>"; 
    ?>

    <div class='comment-box default-border dragon-background'>
        <h2 class="padding-left-10px">Discussion Thread</h2>
        <?php
            echo getComment($dbLink);
        ?>
    </div>

    <?php
      include('./View/shared/footer.php');
    ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>