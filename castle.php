<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="//db.onlinewebfonts.com/c/527d17cddcb5f301ba9400f40aaf3d84?family=Avalon" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="Images/Midgardsorm_icon.ico">
    <title>Dragalia Lost Castle</title>
</head>
<body>
    <?php
    include('./View/shared/navigation.php');

    echo"
    <section class='bottom-margin top-margin'>
      <div id='Castle' class='text-box default-border default-halftone-effect'>
        <img class='img-fluid img-thumbnail float-right screenshot-img' src='Images/Castle_Grounds_Screenshot.png' alt='Castle Ground Screenshot'>
        <div class='container'>
            <h2>Castle</h2>
            <p>
                The Castle section is the player's home base. Various facilities can be build here that provide useful buffs and resources.
            </p>
            <p>
                Facilities are divided into 5 categories: General, Production, Adventurer, Dragon and Decorative.
            </p>
            <p>
                To be able to build or upgrade a facility, you must have the necessary gold, maeterials and an available Smithwyrm.
                Smithwyrms are small, brown builder Dragons that construct the buildings. At the start of the game, the player is given 2 Smithwyrms, but 3 may be purchased.
                It is highly recommended to purchase all 3 (bringing to a maximum of 5) to increase facility construction rate, which will lead to more resources and buffs.
            </p>
            <p>
                Each facility (with the exception of the Halidom and Smithy) can be upgraded to level 30.
            </p>
        </div>
      </div>
    </section>";
    ?>

    <section class="bottom-margin top-margin">
            <div id="GeneralFacility" class="text-box default-border default-halftone-effect-flip text-align-right">
                <div class="container">
                  <h2>General</h2>
                    <p>
                        The General facilities consist of 2 buildings: Halidom & Smithy.
                    </p>
                    <p>
                        The Halidom is the main building of the Castle section. Upgrading it will increase the Castle Grounds, which allows you to place more building onto your Castle Grounds.
                        Upgrading the Halidom is also a necessary requirement for upgrading the Smithy facility.
                        The maximum level for the Halidom is level 9.
                    </p>
                    <p>
                        The Smithy facility, as the name suggests, is the facility which allows the player to craft and upgrade Adventurer's weaponry.
                        The higher the level, the more options become available. Maximum level for Smithy is 9.
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">
                                <img class="img-fluid" src="Images/Halidom.png" alt="Halidom facility">
                            </div>
                            <div class="col-sm-5">
                                <img class="img-fluid" src="Images/Smithy.png" alt="Smithy facility">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="bottom-margin top-margin">
            <div id="ProductionFacility" class="text-box default-border default-halftone-effect">
                <img class="img-fluid img-thumbnail float-right screenshot-img" src="Images/Mines.png" alt="Rupie Mines and Dragontree Screenshot">
              <div class="container">
                <h2>Production</h2>
                <p>
                    Production facilities include the Rupie Mine and the Dragontree.
                </p>
                <p>
                    Rupie Mines produce Rupies (gold) every few hours.
                </p>
                <p>
                    The Dragontree produces Dragonfruit, which is used to upgrade the level of Dragons.
                </p>
                <p>
                    Upgrading these facilities will increase their production rate and the amount they produce.
                    However, upgrading production facilities is low priority. Gaining gold and Dragonfruit by playing through missions is much more effective.
                    Upgrade these when you have spare gold or have extra materials.
                </p>
              </div>
            </div>
    </section>

    <section class="bottom-margin top-margin">
            <div id="AdventurerFacility" class="text-box default-border default-halftone-effect-flip text-align-right">
                <img class="img-fluid img-thumbnail float-left screenshot-img" src="Images/Dojos_Screenshot.png" alt="Dojos on Castle Grounds Screenshot">
                <div class="container">
                    <h2>Adventurer</h2>
                    <p>
                        The Adventurer facilities are made of Shrines and Dojos.
                    </p>
                    <p>
                        Shrines are alters that boost the stats of Adventurers depending on their element.
                        E.g. a <span class="color-fire">fire</span> Shrine will boost the stats of <span class="color-fire">fire-attuned</span> Adventurers.
                    </p>
                 
                    <p>
                        Dojos are facilities that boost the stats of Adventurers depending on their weapon class.
                        E.g. An axe dojo will increase the stats of Adventurers that wield an Axe weapon.
                    </p>
                    <p>
                        Unlike every other facility, dojos require Insignias and Royal Insignias, which are obtained from <a href="stages.html#ImperialOnslaught">Imperial Onslaught</a> stages.
                        Dojos cannot be purchased from the regular Castle shop, but from the Imperial Onslaught shop.
                    </p>
                    <p>
                        Adventurer facilities are the highest priority facilities one should aim to upgrade.
                        Especially the dojos as they give the biggest boosts.
                    </p>
                </div>
            </div>
    </section>


    <section class="bottom-margin top-margin">
            <div id="DragonFacility" class="text-box default-border default-halftone-effect">
              <div class="container">
                <h2>Dragon</h2>
                <p>
                    Dragon facilities, as their name implys, boost the stats of Dragons depending on their element.
                    The two facilities of this category are: Dracolith's & Fafnir Statues.
                </p>
                <p>
                    Dracoliths are monuments which increase the damage dealt by Dragons.
                    These are unlocked by playing through the campaign.
                    Upgrading Dracoliths requires Dragon Scales and Dragon Spheres. Both are obtained from <a href="stages.html#DragonTrials">Dragon Trials</a>.
                    Currently, there are only Dracoliths for the <span class="color-wind">wind</span>,<span class="color-water">water</span> and <span class='color-fire'>fire</span> element.
                </p>
                <p>
                    Fafnir Statues are monuments only available to those that have conquered the <a href="stages.html#AdvancedDragonTrials">Advanced Dragon Trials</a>.
                    Unlike Dracoliths, Fafnir Statues upgrade both the Strength and Health values of Dragons (again depending on the element).
                    The materials necessary to upgrade them are only available in the <a href="stages.html#AdvancedDragonTrials">Advanced Dragon Trials</a>.
                </p>
              </div>
            </div>
    </section>

    <section class="bottom-margin top-margin">
            <div id="Decoration" class="text-box default-border default-halftone-effect-flip text-align-right">
              <div class="container">
                <h2>Decorative</h2>
                <p>
                    Decorative facilities are simply cosmetical items that decorate the Player's Castle Grounds.
                </p>
                <p>
                    They may be purchased from the Castle shop.
                </p>
              </div>
            </div>
    </section>
    
    

    <?php
      include('./View/shared/footer.php');
    ?>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>