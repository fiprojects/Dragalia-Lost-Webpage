<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="//db.onlinewebfonts.com/c/527d17cddcb5f301ba9400f40aaf3d84?family=Avalon" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="Images/Midgardsorm_icon.ico">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="js/index_video_header.js"></script>
    <title>Dragalia Lost Homepage</title>
</head>
<body>  
    <?php
    include('./View/shared/navigation.php');
    echo"
        <video autoplay muted loop id='headerVideo' class='default-border'>
            <source src='Videos/Homepage_Video.mp4' type='video/mp4'>
        </video>

        <div class='videoController'>
            <h3 id='videoHeaderTitle'>Dragalia Lost Trailer</h3>
            <button id='videoBtn' class='headerVideoBtn' onclick='videoPlayer()' type='button'><i class='fas fa-pause'></i></button>
            <button id='soundBtn' class='headerVideoBtn' onclick='soundPlayer()' type='button'><i class='fas fa-volume-mute'></i></button>
        </div>";
    ?>

    <section class="bottom-margin">
      <div class="text-box default-halftone-effect default-border ">
        <div class="container">
          <h2>About the game</h2>
          <p>
            Dragalia Lost is a free-to-play singleplayer/multyplayer action role-playing game that was developed by Nintendo and Cygames for the Android and iOS.
          </p>
          <p>
            It was released on the 27th September 2018 for the Japanese, American, Taiwan, Hong Kong and Macau region.
          </p>
          <p>
            The main singer and composer for the game is DAOKO.
          </p>
          <p>
            Dragalia Lost is the first collaborative game between Nintendo and Cygames.
          </p>
          <p>
            The game has English and Japanese language support;both fully voice acted.
          </p>
          
        </div>
      </div>
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border default-halftone-effect-flip text-align-right">
        <video  class="float-left video-gameplay" autoplay loop muted controls>
          <source src="Videos/Gameplay.MP4" type="video/mp4">
        </video>
        <div class="container">
          <h2>Gameplay</h2>
          <p>
            In Dragalia Lost you use touchscreen controls to control the characters, known as <a href="adventurers.html#Adventurers">Adventurers</a>.
          </p>
          <p>
            Attacking is achieved by simply tapping (normal attacks), holding down & then releasing(force strike attack) or by using abilities(primary,secondary abilities).
            Another way of attacking is the <a href="dragons.html#Dragons">Dragon form</a>, which becomes available after filling the <a href="dragons.html#Dragons">Dragon gauge</a>.
            There are different type of <a href="stages.html">stages</a> that have various objectives ,but most stages are won by defeating the boss at the end of them.
          </p>
        </div>
      </div>  
    </section>

    <section class="bottom-margin top-margin">
      <div class="text-box default-border default-halftone-effect text-align-left">
        <img class="img-fluid img-thumbnail float-right screenshot-small-img" src="Images/Summon_Results.png" alt="Summoning Results">
        <div id="Summoning" class="container">
          <h2>Summoning</h2>
          <p>
            Before getting into Dragalia Lost, it is worth noting that this game features in-game purchases.
            These in-game purchases come in the form of the Summoning system (popularly known as Gacha).
          </p>
          <p>
            All key elements like <a href="adventurers.html#Adventurers">Adventurers</a>,<a href="dragons.html#Dragons">Dragons</a> & <a href="wyrmprints.html">Wyrmprints</a> come from this Summoning system.
            Every one of these has their own rarity (3&#9733; are common, 4&#9733; are uncommon and 5&#9733; are rare) with each having different appeareance rates.
          </p>
          <p>
            Summoning requires Wyrmrite, Summoning Vouchers or Diamantium.
            Wyrmrite & Summoning Vouchers can be obtained by simply regularly playing and clearing stages, while Diamantium is a special currency which can be exchanged with real currency.
          </p>
          <p>
            Duplicates Adventurers, Dragons & Wyrmprints can be obtained. However, duplicate Adventurers are converted into Eldwater currency which can be used to upgrade their <a href="adventurers.html#Adventurers">Co-abilities</a>.
          </p>
        </div>
      </div>
    </section>

    <?php
      include('./View/shared/footer.php');
    ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>