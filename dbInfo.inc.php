<?php
    $DBHost = "localhost";
    $DBUsername = "";
    $DBPassword = "";
    $DBName = "";

    $dbLink = mysqli_connect($DBHost, $DBUsername, $DBPassword);

    if(!$dbLink) {
        die("Can't connect...");
    }
    mysqli_select_db($dbLink, $DBName);
?>