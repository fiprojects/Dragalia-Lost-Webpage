<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <img class="nav-logo" src="Images/Logo.png" alt="Dragalia Lost Logo">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="index.php">Homepage</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="adventurers.php">Adventurer</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="dragons.php">Dragons</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="stages.php">Stages</a>
                    </li>
                    <li class="nav-item active">
                      <a class="nav-link" href="castle.php">Castle</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="wyrmprints.php">Wyrmprints</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="comments.php">Comments (Deprecated)</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="resources.php">Resources</a>
                    </li>
                  </ul>
                </div>
              </nav>