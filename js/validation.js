window.onload = function(){
    document.forms.commentForm.onsubmit = function(){
        const nameBox = document.getElementById("name");
        const commentBox = document.getElementById("comment");
    
        if(nameBox.value == ""){
            alert("Name cannot be left blank!");
            nameBox.focus();
            nameBox.style.border = "solid 2px red";
            return false;
        }
        if(commentBox.value == ""){
            alert("Comment cannot be blank.");
            commentBox.focus();
            commentBox.style.border = "solid 2px red";
            return false;
        }
    }
}

