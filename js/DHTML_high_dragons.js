var dragons = ["../Images/High_Midgardsorm.png","../Images/High_Brunhilda.png","../Images/High_Mercury.png"];
var time = 5000;
var i = 0;
var length = dragons.length - 1;

// Change the image
function changeImg(el){
    i += el;
    if(i > length){
        i = 0;
    }
    if(i < 0){
        i = length;
    }
    document.getElementById("HighDragonSlideShow").src = dragons[i];
}

function run(){
    setInterval("changeImg(1)", time);
}
window.onload = run;
