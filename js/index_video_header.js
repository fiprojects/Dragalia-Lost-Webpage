
function videoPlayer(){
    var video = document.getElementById("headerVideo");
    var btn = document.getElementById("videoBtn");

    if(video.paused){
        video.play();
        btn.innerHTML = '<i class="fas fa-pause"></i>';
    }
    else{
        video.pause();
        btn.innerHTML = '<i class="fas fa-play"></i>';
    }
}
function soundPlayer(){
    var video = document.getElementById("headerVideo");
    var btn = document.getElementById("soundBtn");

    if(video.muted == true){
        video.muted = false;
        btn.innerHTML = '<i class="fas fa-volume-up"></i>';
    }
    else{
        video.muted = true;
        btn.innerHTML = '<i class="fas fa-volume-mute"></i>';
    }
}
